# Aether Wallet architecture document

## Project Documentation
1. [Vision document](https://ybin.me/p/63a0d7de7dfe7865#X+YjzMUtrhR96TRtU17iMWzES7TNrMj6rVVAwa7P3pk=)
1. [Roadmap Proposal v0.2](https://docs.google.com/document/d/1qcjefpQecDQvj_J9uft836kLrf2e8u-bLWKWyXRYENQ/edit?usp=sharing)
1. [Architecture document](https://gitlab.com/aetherwallet/docs)
1. [The code](https://gitlab.com/aetherwallet)

## High Level Project Description
Aether wallet is an open-source ESP32 hardware wallet with an OLED screen for user generated mnemonic keys that are stored on the device and and can be used for Ethereum and Bitcoin transactions and message signing.
It features a web interface accessible via WiFi, USB or BT and is costs under 10 Euros per device.


## Product Backlog
1. BIP32/BIP39/BIP44 compliant keys
1. Use Hierarchical Deterministic Wallets
1. OLED GUI
1. HTML GUI
1. Ethereum support
1. Bitcoin support
1. Polkadot support 
1. BCH support
1. EOS support
1. QR code display in OLED 
1. QR code display in web interface
1. Samir Secret Sharing based key recovery


## Architecture Catalogue

### Device
| SSD1306 | ESP32 |
|---	  |---	  |
| [ThingPulse](https://github.com/ThingPulse/esp8266-oled-ssd1306) | Arduino [board definitions](https://dl.espressif.com/dl/package_esp32_index.json)|

### Cryptographic libraries
| TLS / SSL | keccak (sha3) | secp256k1 |
|---		|---			|---		|
| [trezor crypto](https://github.com/trezor/trezor-crypto) |  [trezor crypto](https://github.com/trezor/trezor-crypto) |  [trezor crypto](https://github.com/trezor/trezor-crypto) |
| [mbed tls](https://tls.mbed.org/api/) | [mbed tls](https://github.com/espressif/arduino-esp32/blob/39fb8c30440a4abd5fe0e2c87609ba6798ae8013/tools/sdk/include/mbedtls/mbedtls/md.h) | |
| [NaCI](https://nacl.cr.yp.to/) | | |
| [arduino crypto](https://rweather.github.io/arduinolibs/crypto.html)	| | |
| | [libkeccak](https://github.com/maandree/libkeccak) | |
| | [tiny keccak](https://github.com/coruus/keccak-tiny/blob/940e97a53478fe1ba7990d7784be8782a3ca9451/keccak-tiny.c) | |
| |	| [ethereum arduino](https://github.com/kvhnuke/Ethereum-Arduino/blob/master/Ethereum-Arduino/libs/ecdsa.c)| 
| |	| [libsecp256k1](https://github.com/bitcoin-core/secp256k1) |
|	   |		  |		|

### Middleware
| WEB3 | JSON-RPC | RLP |
|---   |---   	  |---	|
| [web3 arduino](https://github.com/kopanitsa/web3-arduino) | [libjson-rpc](https://github.com/cinemast/libjson-rpc-cpp)	| [ethereum arduino](https://github.com/kvhnuke/Ethereum-Arduino/blob/master/Ethereum-Arduino/RLP.cpp) |
| | [arduino json](https://github.com/bblanchon/ArduinoJson) | |
| | [cJSON](https://github.com/DaveGamble/cJSON) | |		

### Feature
| QR Codes | Samir Secret Sharing |
|---	   |---	  				  |
| [ricmoo's QR Code](https://github.com/ricmoo/QRCode) | [Trezor](https://github.com/trezor/python-shamir-mnemonic) |
|	   |		  |		|

### Crypto libraries
Rhys Weatherley's [arduino cryptolibs](https://github.com/rweather/arduinolibs/tree/master/libraries/Crypto) and [documentation](https://rweather.github.io/arduinolibs/crypto.html)
mbed TLS [sdk](https://github.com/espressif/arduino-esp32/tree/master/tools/sdk/include/mbedtls)


## Development environment
Espressif IDF [Github](https://github.com/espressif/arduino-esp32)


## Tooling
[Arduino IDE](https://www.arduino.cc/en/main/software)
[Qemu](https://www.qemu.org/)
[ESP32-IDF guide](https://docs.espressif.com/projects/esp-idf/en/latest/index.html)
[FreeRTOS guide](https://www.freertos.org/Documentation/161204_Mastering_the_FreeRTOS_Real_Time_Kernel-A_Hands-On_Tutorial_Guide.pdf)
## Resources

### Aleth C++ Ethereum client
[Documentation](http://www.ethdocs.org/en/latest/ethereum-clients/cpp-ethereum/architecture.html)
[Github](https://github.com/ethereum/aleth)
Ethereum book [chapter 4 - keys and addresses](https://github.com/ethereumbook/ethereumbook/blob/develop/04keys-addresses.asciidoc)


###  Tutorials
Using sha256 with mbed tls in [arduino](https://techtutorialsx.com/2018/05/10/esp32-arduino-mbed-tls-using-the-sha-256-algorithm/)
Espressif's [examples](https://github.com/espressif/esp-idf/tree/master/examples)
Nick Kolban's ESP32 [snippets](https://github.com/nkolban/esp32-snippets)
Nick Kolban's ESP32 [youtube series](https://www.youtube.com/watch?v=C2xF3O6qkbg&list=PLB-czhEQLJbWMOl7Ew4QW1LpoUUE7QMOo)

### Similar projects
[Jolt wallet](https://github.com/joltwallet)
[Dark wallet](https://github.com/darkwallet/)
[Firefly](https://github.com/firefly/wallet)
[Anyledger](https://github.com/bdjukic/anyledger-prototype)

### Create an Ethereum address from scratch
	#!/bin/sh

	keys=$(openssl ecparam -name secp256k1 -genkey -noout | openssl ec -text -noout 2> /dev/null)

	# extract private key in hex format, removing newlines, leading zeroes and semicolon
	priv=$(printf "%s\n" $keys | grep priv -A 3 | tail -n +2 | tr -d '\n[:space:]:' | sed 's/^00//')

	# make sure priv has correct length
	if [ ${#priv} -ne 64 ]; then
	    echo "length error"
	    exit
	fi

	# extract public key in hex format, removing newlines, leading '04' and semicolon
	pub=$(printf "%s\n" $keys | grep pub -A 5 | tail -n +2 | tr -d '\n[:space:]:' | sed 's/^04//')

	# get the keecak hash, removing the trailing ' -' and taking the last 40 chars
	# dependency: https://github.com/maandree/sha3sum
	addr=0x$(echo $pub | /usr/local/bin/keccak-256sum -x -l | tr -d ' -' | tail -c 41)

	echo 'Private key:' $priv
	echo 'Public key: ' $pub
	echo 'Address:    ' $addr
    

