#
# This is a project Makefile. It is assumed the directory this Makefile resides in is a
# project subdirectory.
#

PROJECT_NAME := aether-core

CFLAGS +=  \
	 -D U8G2_16BIT \
	 -D ESP_PLATFORM

include $(IDF_PATH)/make/project.mk

