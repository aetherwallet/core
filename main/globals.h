#include "freertos/FreeRTOS.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"
#include "freertos/task.h"

#include "u8g2.h"

#include "gui/menu8.h"

extern volatile u8g2_t u8g2;
extern volatile m8_t menu8;
extern volatile SemaphoreHandle_t menu8_mutex;
extern volatile QueueHandle_t input_queue;
