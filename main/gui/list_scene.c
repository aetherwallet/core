#include "list_scene.h"
#include "esp_log.h"
#include "oled.h"
#include "gui.h"

static const char *TAG = "list_scene";

#define LIST_SCENE_ITEM_WIDTH OLED_WIDTH
#define LIST_SCENE_ITEM_HEIGHT 12
#define LIST_SCENE_ITEM_COUNT 4

void list_scene_draw(m8_cb_args_t *arguments) {
    list_scene_data_t *l_scene_data = (list_scene_data_t*)arguments->scene->data;
    ESP_LOGI(TAG, "drawing list scene:%u item count:%u", arguments->scene->id, l_scene_data->item_count);
    if (!l_scene_data->active) return;
    list_scene_item_t *start_item = l_scene_data->active;
    int i = 0;
    while (start_item->previous && i <= LIST_SCENE_ITEM_COUNT / 2) {
        i++;
        start_item = start_item->previous;
    }
    i = 0;
    uint8_t y = M8_SBAR_HEIGHT + 2;
    while (start_item && i < LIST_SCENE_ITEM_COUNT) {
        if (start_item == l_scene_data->active) {
            u8g2_DrawBox(arguments->u8g2, 0, y, LIST_SCENE_ITEM_WIDTH, LIST_SCENE_ITEM_HEIGHT);
            u8g2_SetDrawColor(arguments->u8g2, 0);
        }
        u8g2_DrawStr(arguments->u8g2,
            (LIST_SCENE_ITEM_WIDTH - u8g2_GetStrWidth(arguments->u8g2, (const char*) start_item->title)) / 2,
            y + (LIST_SCENE_ITEM_HEIGHT - GUI_FONT_HEIGHT )/2 + GUI_FONT_HEIGHT,
            (const char*) start_item->title);
        u8g2_SetDrawColor(arguments->u8g2, 1);
        start_item = start_item->next;
        y += LIST_SCENE_ITEM_HEIGHT;
        i++;
    }
}

void list_scene_up(m8_cb_args_t *arguments) {
    list_scene_data_t *l_scene_data = (list_scene_data_t*)arguments->scene->data;
    if (l_scene_data->active && l_scene_data->active->previous)
        l_scene_data->active = l_scene_data->active->previous;
}

void list_scene_down(m8_cb_args_t *arguments) {
    list_scene_data_t *l_scene_data = (list_scene_data_t*)arguments->scene->data;
    if (l_scene_data->active && l_scene_data->active->next)
        l_scene_data->active = l_scene_data->active->next;
}

void list_scene_item_add(m8_scene_t *l_scene, list_scene_item_t *l_scene_item) {
    ESP_LOGI(TAG, "adding item to list scene %u", l_scene->id);
    l_scene_item->next = NULL;
    list_scene_data_t *l_scene_data = (list_scene_data_t*)l_scene->data;
    if (l_scene_data->head) {
        ESP_LOGI(TAG, "finding place for item:%s", l_scene_item->title);
        list_scene_item_t *item_it = l_scene_data->head;
        while (item_it->next) item_it = item_it->next;
        ESP_LOGI(TAG, "found after:%s", item_it->title);
        l_scene_item->previous = item_it;
        item_it->next = l_scene_item;
    } else {
        ESP_LOGI(TAG, "adding item:%s to the scene head", l_scene_item->title);
        l_scene_item->previous = NULL;
        l_scene_data->head = l_scene_item;
        l_scene_data->active = l_scene_item;
    }
    l_scene_data->item_count++;
}

void list_scene_init(m8_scene_t *l_scene) {
    ESP_LOGI(TAG, "initializign list scene %u", l_scene->id);
    l_scene->show = NULL;
    l_scene->draw = list_scene_draw;
    l_scene->hide = NULL;
    l_scene->up = list_scene_up;
    l_scene->down = list_scene_down;
    l_scene->left = NULL;
    l_scene->right = NULL;
    l_scene->enter = NULL;
    l_scene->data = (void*)malloc(sizeof(list_scene_data_t));
    list_scene_data_t *l_scene_data = (list_scene_data_t*)l_scene->data;
    l_scene_data->item_count = 0;
    l_scene_data->head = NULL;
}
