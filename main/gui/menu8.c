#include "menu8.h"
#include "gui.h"
#include "oled.h"
#include "esp_log.h"

static const char *TAG = "menu8";
void m8_sbar_init(m8_t*);

void m8_down(m8_t *menu) {
    xSemaphoreTake(menu->mutex, portMAX_DELAY);
    if (menu->active_scene && menu->active_scene->up) {
        m8_cb_args_t args = {
            .u8g2 = menu->u8g2,
            .scene = menu->active_scene
        };
        menu->active_scene->down(&args);
    }
    xSemaphoreGive(menu->mutex);
}

void m8_up(m8_t *menu) {
    xSemaphoreTake(menu->mutex, portMAX_DELAY);
    if (menu->active_scene && menu->active_scene->up) {
        m8_cb_args_t args = {
            .u8g2 = menu->u8g2,
            .scene = menu->active_scene
        };
        menu->active_scene->up(&args);
    }
    xSemaphoreGive(menu->mutex);
}

m8_error_t m8_scene_set_active(m8_t *menu, uint8_t id) {
    m8_scene_t *previous_scene = menu->active_scene;
    if ((menu->active_scene = m8_scene_get(menu, id))) {
        m8_sbar_init(menu); 
        m8_cb_args_t args = {
            .u8g2 = menu->u8g2,
            .scene = previous_scene
        };
        if (previous_scene && previous_scene->hide) {
            ESP_LOGI(TAG, "calling prev scene hide");
            previous_scene->hide(&args);
        }
        if (menu->active_scene->show) {
            ESP_LOGI(TAG, "calling new scene show");
            args.scene = menu->active_scene;
            menu->active_scene->show(&args);
        }
        return M8_SUCCESS;
    }
    ESP_LOGW(TAG, "scene with id:%u doest exsists", id);
    return M8_ERROR;
}

m8_error_t m8_scene_add(m8_t *menu, m8_scene_t *scene) {
    ESP_LOGI(TAG, "adding new menu scene");
    if (menu->scene_count >= menu->scene_max_count) {
        ESP_LOGW(TAG, "menu scene is already full");
        return M8_ERROR;
    }
    if (m8_scene_get(menu, scene->id)) {
        ESP_LOGW(TAG, "scene exsists with this id");
        return M8_ERROR;
    }
    menu->scenes[menu->scene_count++] = scene;
    return M8_SUCCESS;
}

m8_scene_t* m8_scene_get(m8_t *menu, uint8_t id) {
    ESP_LOGI(TAG, "checking if scene exsists with same id");
    m8_scene_t **scene = menu->scenes;
    ESP_LOGI(TAG, "iterating over pointers to scenes");
    while (*scene && (*scene)->id != id) scene++;
    return *scene;
}

void m8_sbar_battery_draw(m8_t *menu) {
    /*xSemaphoreTake(menu->mutex, portMAX_DELAY);*/
    ESP_LOGI(TAG, "drawing battery identificator");
    uint8_t icon_offset = OLED_WIDTH - menu->sbar.battery.icon->width - 1;
    u8g2_DrawXBM(
            menu->u8g2,
            icon_offset,
            0,
            menu->sbar.battery.icon->width,
            menu->sbar.battery.icon->height,
            menu->sbar.battery.icon->data);
    ESP_LOGI(TAG, "drawing battery level");
    //calculate offset of battery percentage text
    icon_offset = OLED_WIDTH - 3;
    icon_offset -= menu->sbar.battery.icon->width;
    icon_offset -= u8g2_GetStrWidth(menu->u8g2, (const char*) menu->sbar.battery.level_str);
    u8g2_DrawStr(menu->u8g2, icon_offset, 9, (const char*) menu->sbar.battery.level_str);
    /*xSemaphoreGive(menu->mutex);*/
}

void m8_sbar_left_icons_draw(m8_t *menu) {
    ESP_LOGI(TAG, "drawing all sbar left icons");
    uint8_t i = 0;
    uint8_t icon_offset = 0;
    for(; i < M8_MAX_SBAR_ICON_COUNT; i++) {
        if (menu->sbar.icons[i]) {
            u8g2_DrawXBM(
                    menu->u8g2,
                    icon_offset,
                    1,
                    menu->sbar.icons[i]->width,
                    menu->sbar.icons[i]->height,
                    menu->sbar.icons[i]->data);
            icon_offset += menu->sbar.icons[i]->width + 1;
        } else ESP_LOGW(TAG, "sbar icon is null");
    }
}

void m8_sbar_draw(m8_t *menu) {
	ESP_LOGI(TAG, "drawing status bar hline");
	//we add +1 to y position to make sure
	//status bar always gets height specified in macro
    u8g2_DrawHLine(menu->u8g2, 0, M8_SBAR_HEIGHT + 1, OLED_WIDTH);
    m8_sbar_left_icons_draw(menu);
	ESP_LOGI(TAG, "checking if status bar has title");
    if (menu->sbar.title) {
        ESP_LOGI(TAG, "drawing status bar title");
        u8g2_DrawStr(menu->u8g2,
                (OLED_WIDTH - u8g2_GetStrWidth(menu->u8g2, (const char*) menu->sbar.title))/2,
                9,
                (const char*) menu->sbar.title);
    } else ESP_LOGW(TAG, "status bar has no title");
    m8_sbar_battery_draw(menu);
    ESP_LOGI(TAG, "ended drawing status bar");
}

void m8_draw(m8_t* menu) {
    xSemaphoreTake(menu->mutex, portMAX_DELAY);
	ESP_LOGI(TAG, "starting menu draw, clearing u8g2 buffer");
    u8g2_ClearBuffer(menu->u8g2);
	ESP_LOGI(TAG, "calling m8 status bar draw");
    m8_sbar_draw(menu);

    ESP_LOGI(TAG, "drawing active scene if one is present");
    if (menu->active_scene && menu->active_scene->draw) {
            ESP_LOGI(TAG, "creating arguments struct to pass to call scene draw");
            m8_cb_args_t args = {
                .u8g2 = menu->u8g2,
                .scene = menu->active_scene
            };
            menu->active_scene->draw(&args);
    } else ESP_LOGW(TAG, "no active scene or no draw is presented");

	ESP_LOGI(TAG, "ending menu draw, sending u8g2 buffer");
    u8g2_SendBuffer(menu->u8g2);
    xSemaphoreGive(menu->mutex);
}

void m8_battery_state_update(m8_t *menu) { 
    ESP_LOGI(TAG, "initializign sbar battery lvl status and icon");
    battery_get_lvl_string(menu->sbar.battery.level_str);
    menu->sbar.battery.status = battery_get_status();
    menu->sbar.battery.icon = menu->sbar.battery.status == B_CHARGING
        ? &ac_icon_xbm
        : &battery_icon_xbm;
}

void m8_sbar_init(m8_t *menu) {
	ESP_LOGI(TAG, "setting sbar title if active scene has one");
	if (menu->active_scene) menu->sbar.title = menu->active_scene->title;
	ESP_LOGI(TAG, "allocating sbar icon array");
    menu->sbar.icons = (const bitmap_t**)malloc(sizeof(bitmap_t*) * M8_MAX_SBAR_ICON_COUNT);
    menu->sbar.icons[0] = &wifi_icon_xbm;
    menu->sbar.icons[1] = NULL;
    menu->sbar.icons[2] = NULL;
    m8_battery_state_update(menu);
    ESP_LOGI(TAG, "setting sbar height");
    menu->sbar.height = M8_SBAR_HEIGHT;
}

void m8_init(m8_t *menu, u8g2_t *u8g2, SemaphoreHandle_t mutex, uint8_t scene_max_count) {
    xSemaphoreTake(mutex, portMAX_DELAY);
	ESP_LOGI(TAG, "setting u8g2 context and menu mutex");
    menu->u8g2 = u8g2;
    menu->mutex = mutex;
	ESP_LOGI(TAG, "allocating memory for scenes");
    menu->scenes = (m8_scene_t**)malloc(sizeof(m8_scene_t*) * scene_max_count);
    ESP_LOGI(TAG, "setting all scene pointers to NULL");
    int i = 0;
    for (; i < scene_max_count; i++)
        menu->scenes[i] = NULL;
    menu->scene_max_count = scene_max_count;
    menu->active_scene = NULL;
	ESP_LOGI(TAG, "calling sbar init function");
	m8_sbar_init(menu);
    xSemaphoreGive(mutex);
}
