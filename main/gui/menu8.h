#ifndef MENU8_H
#define MENU8_H
#include "freertos/FreeRTOS.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"
#include "freertos/task.h"
#include "u8g2.h"

#include "../battery/battery.h"
#include "bitmaps.h"

#define M8_MAX_SBAR_ICON_COUNT 3
#define M8_SBAR_HEIGHT 10
#define M8_SCENE_TITLE_LENGTH 11

typedef enum { M8_SUCCESS = 0, M8_ERROR } m8_error_t;

typedef struct m8_cb_args_t {
    u8g2_t *u8g2;
    struct m8_scene_t *scene;
} m8_cb_args_t;

typedef void (*m8_cb_t)(m8_cb_args_t*);

typedef struct m8_battery_t {
    char level_str[5];
    battery_status_t status;
    const bitmap_t *icon;
} m8_battery_t;

typedef struct m8_sbar_t {
    const bitmap_t **icons;
    m8_battery_t battery;
    uint8_t height;
    char *title;
} m8_sbar_t;

typedef struct m8_scene_t {
    uint8_t id;
    void *data;
    char title[M8_SCENE_TITLE_LENGTH];
    m8_cb_t show;
    m8_cb_t draw;
    m8_cb_t hide;
    m8_cb_t up;
    m8_cb_t down;
    m8_cb_t left;
    m8_cb_t right;
    m8_cb_t enter;
} m8_scene_t;

typedef struct m8_t {
    u8g2_t *u8g2;
    SemaphoreHandle_t mutex;
    m8_sbar_t sbar;
    m8_scene_t **scenes;
    m8_scene_t *active_scene;
    uint8_t scene_max_count;
    uint8_t scene_count;
} m8_t;

m8_error_t m8_scene_set_active(m8_t *menu, uint8_t id);
m8_scene_t* m8_scene_get(m8_t *menu, uint8_t id);
m8_error_t m8_scene_add(m8_t *menu, m8_scene_t *scene);

void m8_battery_state_update(m8_t *menu);
void m8_up(m8_t *menu);
void m8_down(m8_t *menu);
void m8_draw(m8_t *menu);
void m8_init(m8_t *menu, u8g2_t *u8g2, SemaphoreHandle_t mutex, uint8_t scene_max_count);

#endif
