#ifndef GUI_H
#define GUI_H

#include "u8g2.h"

#define GUI_FONT_HEIGHT 8

void gui_setup_screen(u8g2_t *u8g2);

#endif
