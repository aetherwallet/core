#include "gui.h"
#include "oled.h"
#include "u8g2.h"
#include "../hal/u8g2_esp32.h"
#include "esp_log.h"

static const char *TAG = "gui";

void gui_setup_screen(u8g2_t *u8g2) {
    // Initialize OLED Screen I2C params
	ESP_LOGI(TAG, "Starting  hal");
    u8g2_esp32_hal_t u8g2_esp32_hal = U8G2_ESP32_HAL_DEFAULT;
    u8g2_esp32_hal.sda  = OLED_SDA_PIN;
    u8g2_esp32_hal.scl  = OLED_SCL_PIN;
	ESP_LOGI(TAG, "Starting initializing hal");
    u8g2_esp32_hal_init(u8g2_esp32_hal);
   
	ESP_LOGI(TAG, "Configure u8g2 ssd1306");
    u8g2_Setup_ssd1306_i2c_128x64_noname_f(
        u8g2,
        U8G2_R0,
        u8g2_esp32_i2c_byte_cb,
        u8g2_esp32_gpio_and_delay_cb
    );  // init u8g2 structure

	ESP_LOGI(TAG, "Set I2C");
    // Note: SCREEN_ADDRESS is already shifted left by 1
    u8x8_SetI2CAddress(&(u8g2->u8x8), 0x78);

	ESP_LOGI(TAG, "Set Display");
    u8g2_InitDisplay(u8g2);
	ESP_LOGI(TAG, "Set powersave");
    u8g2_SetPowerSave(u8g2, 0); // wake up display
	ESP_LOGI(TAG, "Clear dsp");
    u8g2_ClearDisplay(u8g2);
	ESP_LOGI(TAG, "Clear buffer");
    u8g2_ClearBuffer(u8g2);

    u8g2_SetContrast(u8g2, 255);
    u8g2_SetFont(u8g2, u8g2_font_t0_11_tf);
}
