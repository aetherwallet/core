#ifndef LIST_SCENE_H
#define LIST_SCENE_H

#include "menu8.h"

#define LIST_SCENE_ITEM_TITLE_LENGTH 15

typedef void (*list_scene_cb_t)();

typedef struct list_scene_item_t {
    char title[LIST_SCENE_ITEM_TITLE_LENGTH];
    const bitmap_t *icon;
    struct list_scene_item_t *previous;
    struct list_scene_item_t *next;
    list_scene_cb_t enter;
} list_scene_item_t;

typedef struct {
    uint8_t item_count;
    list_scene_item_t *head;
    list_scene_item_t *active;
} list_scene_data_t;

void list_scene_init(m8_scene_t *l_scene);
void list_scene_item_add(m8_scene_t *l_scene, list_scene_item_t *l_scene_item);

#endif
