#ifndef BITMAP_H
#define BITMAP_H

#include <stdint.h>

static const uint8_t wifi_icon_xbm_data[] = {
    0x00, 0x00, 0x00, 0x00,
    0x78, 0x00, 0x84, 0x00,
    0x02, 0x01, 0x79, 0x02,
    0x84, 0x00, 0x30, 0x00,
    0x30, 0x00, 0x00, 0x00
};

static const uint8_t battery_icon_xbm_data[] = {
    0x1c, 0x63, 0x41, 0x5d,
    0x41, 0x5d, 0x41, 0x5d,
    0x41, 0x3e
};

static const uint8_t ac_icon_xbm_data[] = {
   0x30, 0x18, 0x0c, 0x06,
   0x3e, 0x20, 0x30, 0x18,
   0x0c, 0x06
};

typedef struct {
    uint8_t width;
    uint8_t height;
    const uint8_t *data;
} bitmap_t;

static const bitmap_t wifi_icon_xbm = {
    .width = 10,
    .height = 10,
    .data = (const uint8_t*)&wifi_icon_xbm_data
};

static const bitmap_t battery_icon_xbm = {
    .width = 7,
    .height = 10,
    .data = (const uint8_t*)&battery_icon_xbm_data
};

static const bitmap_t ac_icon_xbm = {
    .width = 7,
    .height = 10,
    .data = (const uint8_t*)&ac_icon_xbm_data
};

#endif
