#include "battery.h"
#include <stdlib.h>
#include "esp_system.h"

uint8_t battery_get_lvl() {
    return (uint8_t)(esp_random() % 100);
}

battery_status_t battery_get_status() {
    return esp_random() % 2 ? B_DISCHARGING : B_CHARGING;
}

void battery_get_lvl_string(char *dest) {
    uint8_t lvl = battery_get_lvl();
    utoa((unsigned int)lvl, dest, 10);
    uint8_t index = lvl < 100 ? (lvl < 10 ? 1 : 2) : 3;
    dest[index++] = '%';
    dest[index] = '\0';
}
