#ifndef BATTERY_H
#define BATTERY_H

#include <stdint.h>

typedef enum { B_CHARGING = 0, B_DISCHARGING } battery_status_t;

uint8_t battery_get_lvl();
void battery_get_lvl_string(char *dest);
battery_status_t battery_get_status();

#endif
