/* Hello World Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include <string.h>

#include "freertos/FreeRTOS.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"
#include "freertos/task.h"

#include "sdkconfig.h"

#include "esp_system.h"
#include "esp_spi_flash.h"
#include "u8g2.h"
#include "gui/gui.h"
#include "gui/menu8.h"
#include "gui/list_scene.h"
#include "globals.h"
#include "easy_input.h"

volatile u8g2_t u8g2;
volatile m8_t menu8;
volatile QueueHandle_t input_queue;
volatile SemaphoreHandle_t menu8_mutex;

void draw(m8_cb_args_t *arguments) {
    static uint8_t iterator = 3;
    iterator+=1;
    u8g2_DrawCircle(arguments->u8g2, 64, 32, iterator, U8G2_DRAW_ALL);
    if(iterator > 15)
        iterator = 0;
};

void draw2(m8_cb_args_t *arguments) {
    static uint8_t iterator = 3;
    iterator+=1;
    u8g2_DrawFrame(arguments->u8g2, 32, 16, iterator, iterator);
    if(iterator > 20)
        iterator = 0;
};

void hide(m8_cb_args_t *arguments) {
    printf("hiding scene %u", arguments->scene->id);
}

void show(m8_cb_args_t *arguments) {
    printf("showing scene %u", arguments->scene->id);
}

void addTestScenes(m8_t *menu) {
    m8_scene_t *scene = (m8_scene_t*)malloc(sizeof(m8_scene_t));
    scene->id = 1;
    scene->draw = draw;
    scene->hide = hide;
    scene->show = show;
    strcpy(scene->title, "Circle Menu");
    m8_scene_add(menu, scene);
    m8_scene_t *scene2 = (m8_scene_t*)malloc(sizeof(m8_scene_t));
    scene2->id = 2;
    scene2->draw = draw2;
    scene2->hide = hide;
    scene2->show = show;
    strcpy(scene2->title, "Square Menu");
    m8_scene_add(menu, scene2);
}

void onMenuItemPress() {
    printf("menu item press");
}

void addListScene(m8_t *menu) {
    m8_scene_t *l_scene = (m8_scene_t*)malloc(sizeof(m8_scene_t));
    l_scene->id = 3;
    strcpy(l_scene->title, "List Menu");
    list_scene_init(l_scene);

    list_scene_item_t *l_scene_item1 = (list_scene_item_t*)malloc(sizeof(list_scene_item_t));
    strcpy(l_scene_item1->title, "Menu Item 1");
    l_scene_item1->enter = onMenuItemPress;

    list_scene_item_t *l_scene_item2 = (list_scene_item_t*)malloc(sizeof(list_scene_item_t));
    strcpy(l_scene_item2->title, "Menu Item 2");
    l_scene_item2->enter = onMenuItemPress;

    list_scene_item_t *l_scene_item3 = (list_scene_item_t*)malloc(sizeof(list_scene_item_t));
    strcpy(l_scene_item3->title, "Menu Item 3");
    l_scene_item3->enter = onMenuItemPress;

    list_scene_item_t *l_scene_item4 = (list_scene_item_t*)malloc(sizeof(list_scene_item_t));
    strcpy(l_scene_item4->title, "Menu Item 4");
    l_scene_item4->enter = onMenuItemPress;

    list_scene_item_t *l_scene_item5 = (list_scene_item_t*)malloc(sizeof(list_scene_item_t));
    strcpy(l_scene_item5->title, "Menu Item 5");
    l_scene_item5->enter = onMenuItemPress;

    list_scene_item_t *l_scene_item6 = (list_scene_item_t*)malloc(sizeof(list_scene_item_t));
    strcpy(l_scene_item6->title, "Menu Item 6");
    l_scene_item6->enter = onMenuItemPress;

    list_scene_item_t *l_scene_item7 = (list_scene_item_t*)malloc(sizeof(list_scene_item_t));
    strcpy(l_scene_item7->title, "Menu Item 7");
    l_scene_item7->enter = onMenuItemPress;

    list_scene_item_t *l_scene_item8 = (list_scene_item_t*)malloc(sizeof(list_scene_item_t));
    strcpy(l_scene_item8->title, "Menu Item 8");
    l_scene_item8->enter = onMenuItemPress;

    list_scene_item_add(l_scene, l_scene_item1);
    list_scene_item_add(l_scene, l_scene_item2);
    list_scene_item_add(l_scene, l_scene_item3);
    list_scene_item_add(l_scene, l_scene_item4);
    list_scene_item_add(l_scene, l_scene_item5);
    list_scene_item_add(l_scene, l_scene_item6);
    list_scene_item_add(l_scene, l_scene_item7);
    list_scene_item_add(l_scene, l_scene_item8);
    m8_scene_add(menu, l_scene);
}

void input_task() {
    uint64_t input_buf;
    while(1){
        // Block until user inputs a button
        if(xQueueReceive(input_queue, &input_buf, portMAX_DELAY)) {
            printf("User Input #%lld: 0b", input_buf);
            printf("\n");
            if(input_buf == 1)
                m8_up((m8_t *) &menu8);
            else if(input_buf == 2)
                m8_down((m8_t *) &menu8);
            m8_battery_state_update((m8_t *) &menu8);
            m8_draw((m8_t *) &menu8);
        }
    }
}

void input_task_up() {
    while(true) {
        printf("Up Up\n");
        m8_up((m8_t *) &menu8);
        m8_draw((m8_t *) &menu8);
        vTaskDelay(200);
    }
}

void input_task_down() {
    while(true) {
        printf("Down Down\n");
        m8_down((m8_t *) &menu8);
        m8_draw((m8_t *) &menu8);
        vTaskDelay(200);
    }
}

void app_main()
{
    easy_input_queue_init((QueueHandle_t*)&input_queue);
    easy_input_run((QueueHandle_t*)&input_queue);
    menu8_mutex = xSemaphoreCreateMutex();
    printf("Hell u8g2  world!\n");
    gui_setup_screen((u8g2_t *) &u8g2);
    m8_init((m8_t *) &menu8, (u8g2_t *) &u8g2, menu8_mutex, 30);
    printf("Hello world!\n");
    /*[> Print chip information <]*/
    esp_chip_info_t chip_info;
    esp_chip_info(&chip_info);
    printf("This is ESP32 chip with %d CPU cores, WiFi%s%s, ",
            chip_info.cores,
            (chip_info.features & CHIP_FEATURE_BT) ? "/BT" : "",
            (chip_info.features & CHIP_FEATURE_BLE) ? "/BLE" : "");

    printf("silicon revision %d, ", chip_info.revision);

    printf("%dMB %s flash\n", spi_flash_get_chip_size() / (1024 * 1024),
            (chip_info.features & CHIP_FEATURE_EMB_FLASH) ? "embedded" : "external");
    addTestScenes((m8_t *) &menu8);
    addListScene((m8_t*) &menu8);
    m8_scene_set_active((m8_t *) &menu8, 3);
    /*int i = 0, e = 1;*/
    /*while(true) {*/
        /*m8_draw((m8_t *) &menu8);*/
        /*vTaskDelay(100);*/
        /*if (e > 0) m8_down((m8_t *) &menu8);*/
        /*else  m8_up((m8_t *) &menu8);*/
        /*i+=e;*/
        /*if (i > 6) {*/
            /*e = -1;*/
        /*}*/
        /*if (i == 0) {*/
            /*e = 1;*/
        /*}*/
    /*}*/

    /*while (false) {*/
        /*m8_draw((m8_t *) &menu8);*/
        /*vTaskDelay(500);*/
        /*m8_scene_set_active((m8_t *) &menu8, 1);*/
        /*m8_scene_set_active((m8_t *) &menu8, 2);*/
        /*m8_draw((m8_t *) &menu8);*/
        /*vTaskDelay(10);*/
    /*}*/

    m8_draw((m8_t *) &menu8);
    xTaskCreate(input_task,
            "INPUT", 3000,
            NULL, 10,
            NULL);
/*    xTaskCreate(input_task_up,*/
            /*"INPUT", 3000,*/
            /*NULL, 10,*/
            /*NULL);*/
    /*xTaskCreate(input_task_down,*/
            /*"INPUT", 3000,*/
            /*NULL, 10,*/
            /*NULL);*/
}
